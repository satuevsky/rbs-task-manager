package models

import (
	"fmt"
	"rbs-task-manager/app"
	"rbs-task-manager/app/models/entities"
)

/**
Модель для работы с задачами
*/
type TaskModel struct{}

/**
Метод для сохранения задачи в БД.
*/
func (t *TaskModel) Save(task entities.Task) error {
	//Если идентификатор не указан, то создаем новую задачу
	if task.Id == 0 {
		_, err := app.Db.Exec("INSERT INTO tasks(name, status, project_id, employee_id) VALUES($1, $2, $3, $4)", task.Name, task.Status, task.ProjectId, task.EmployeeId)
		fmt.Println(task.Name, task.Status, task.ProjectId, task.EmployeeId)
		if err != nil {
			return err
		}
	} else { //Иначе обновляем информацию о задаче
		_, err := app.Db.Exec("UPDATE tasks SET name=$2, status=$3, project_id=$4, employee_id=$5 WHERE id=$1", task.Id, task.Name, task.Status, task.ProjectId, task.EmployeeId)
		if err != nil {
			return err
		}
	}
	return nil
}

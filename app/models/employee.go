package models

import (
	"errors"
	"rbs-task-manager/app"
	"rbs-task-manager/app/models/entities"
)

/**
Модель для работы с Сотрудниками.
*/
type EmployeeModel struct{}

/**
Метод для получения списка всех сотрудников из БД.
*/
func (e *EmployeeModel) GetAll() ([]entities.Employee, error) {
	employees := []entities.Employee{}

	rows, err := app.Db.Query("SELECT id, name, phone, bdate FROM employees")
	if err != nil {
		return employees, err
	}
	defer rows.Close()

	for rows.Next() {
		employee := new(entities.Employee)
		err := rows.Scan(&employee.Id, &employee.Name, &employee.Phone, &employee.Bdate)
		if err != nil {
			return employees, err
		}
		employees = append(employees, *employee)
	}

	return employees, nil
}

/**
Метод для сохранения информации о сотруднике в БД.
*/
func (e *EmployeeModel) Save(employee entities.Employee) error {

	//Если идентификатор сотрудника не указан, то создается новый сотрудник
	if employee.Id == 0 {
		_, err := app.Db.Exec("INSERT INTO employees(name, phone, bdate) VALUES($1, $2, $3)", employee.Name, employee.Phone, employee.Bdate)
		if err != nil {
			return err
		}
	} else { // Иначе обновляется информация о сотруднике
		_, err := app.Db.Exec("UPDATE employees SET name=$2, phone=$3, bdate=$4 WHERE id=$1", employee.Id, employee.Name, employee.Phone, employee.Bdate)
		if err != nil {
			return err
		}
	}

	return nil
}

/**
Метод для удаления сотрудника из БД.
*/
func (e *EmployeeModel) Remove(employeeId int) error {
	//Проверка наличия задач назначенных на этого сотрудника
	rows, err := app.Db.Query("SELECT * FROM tasks WHERE employee_id=$1", employeeId)
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		//Если на данного сотрудника назначена задача, то возвращается ошибка
		return errors.New("На этого сотрудника назначена задача, его нельзя удалить! ")
	}

	//Если задач нет, то удаляем.
	_, err = app.Db.Exec("DELETE FROM employees WHERE id=$1", employeeId)
	if err != nil {
		return err
	}

	return nil
}

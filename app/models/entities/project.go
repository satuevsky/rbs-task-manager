package entities

/**
Базовая структура, описывающая проект
*/
type Project struct {
	Id          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

/**
Структура, описывающая проект со списками задач и сотрудников
*/
type FullProject struct {
	Project
	Tasks     []Task     `json:"tasks"`
	Employees []Employee `json:"employees"`
}

/**
Структура, описывающая проект со списком идентификаторов сотрудников.
Используется при создании проекта.
*/
type ProjectWithEmployeeIds struct {
	Project
	EmployeeIds []int `json:"employeeIds"`
}

package entities

/**
Структура описывающая задачу
*/
type Task struct {
	Id         int    `json:"id"`
	Name       string `json:"name"`
	Status     string `json:"status"`
	EmployeeId int    `json:"employeeId"`
	ProjectId  int    `json:"projectId"`
}

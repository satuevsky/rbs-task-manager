package entities

import "time"

/**
Структура описывающая сотрудника
*/
type Employee struct {
	Id    int       `json:"id"`
	Name  string    `json:"name"`
	Bdate time.Time `json:"bdate"`
	Phone string    `json:"phone"`
}

package models

import (
	"errors"
	"rbs-task-manager/app"
	"rbs-task-manager/app/models/entities"
)

/**
Модель для работы с проектами.
*/
type ProjectModel struct{}

/**
Метод для возврата списка всех проектов из БД.
*/
func (p *ProjectModel) GetAll() ([]entities.Project, error) {
	projects := []entities.Project{}

	rows, err := app.Db.Query("SELECT * FROM projects")
	if err != nil {
		return projects, err
	}
	defer rows.Close()

	for rows.Next() {
		project := new(entities.Project)
		err := rows.Scan(&project.Id, &project.Name, &project.Description)
		if err != nil {
			return projects, err
		}
		projects = append(projects, *project)
	}

	return projects, nil
}

/**
Метод для возврата конкретного проекта со списком его задач и сотрудников.
*/
func (p *ProjectModel) GetProject(id int) (entities.FullProject, error) {
	project := entities.FullProject{Tasks: []entities.Task{}, Employees: []entities.Employee{}}

	//Считывание информации о самом проекте
	row := app.Db.QueryRow("SELECT name, description FROM projects WHERE id=$1", id)
	err := row.Scan(&project.Name, &project.Description)
	if err != nil {
		return project, err
	}

	//Выборка задач проекта
	rows, err := app.Db.Query("SELECT id, name, status, employee_id, project_id FROM tasks WHERE project_id=$1", id)
	if err != nil {
		return project, err
	}
	defer rows.Close()
	for rows.Next() {
		task := new(entities.Task)
		err := rows.Scan(&task.Id, &task.Name, &task.Status, &task.EmployeeId, &task.ProjectId)
		if err != nil {
			return project, err
		}
		project.Tasks = append(project.Tasks, *task)
	}

	//Выборка сотрудников проекта
	rows, err = app.Db.Query("SELECT id, name, phone, bdate FROM project_employees, employees WHERE project_id=$1 AND employee_id=employees.id", id)
	if err != nil {
		return project, err
	}
	defer rows.Close()
	for rows.Next() {
		employee := new(entities.Employee)
		err := rows.Scan(&employee.Id, &employee.Name, &employee.Phone, &employee.Bdate)
		if err != nil {
			return project, err
		}
		project.Employees = append(project.Employees, *employee)
	}

	return project, nil
}

/**
Метод для добавления сотрудника в проект.
*/
func (p *ProjectModel) AddEmployee(projectId, employeeId int) error {
	_, err := app.Db.Exec("INSERT INTO project_employees(project_id, employee_id) VALUES($1, $2)", projectId, employeeId)
	if err != nil {
		return err
	}
	return nil
}

/**
Метод для снятия сотрудника с проекта
*/
//Снимает сотрудника с проекта
func (p *ProjectModel) RemoveEmployee(projectId, employeeId int) error {
	rows, err := app.Db.Query("SELECT * FROM tasks WHERE project_id=$1 AND employee_id=$2", projectId, employeeId)
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		return errors.New("На этого сотрудника назначена задача в данном проекте, его нельзя удалить! ")
	}

	_, err = app.Db.Exec("DELETE FROM project_employees WHERE project_id=$1 AND employee_id=$2", projectId, employeeId)
	if err != nil {
		return err
	}

	return nil
}

//Сохраняет проект в БД.
func (p *ProjectModel) SaveProject(project entities.ProjectWithEmployeeIds) error {
	//Если идентификатор проекта не указан, то создаем его
	if project.Id == 0 {
		//Сохранение самого проекта в бд
		row := app.Db.QueryRow("INSERT INTO projects(name, description) VALUES($1, $2) RETURNING id", project.Name, project.Description)

		//Получения идентификатора сохраненного проекта
		var id int
		err := row.Scan(&id)
		if err != nil {
			return err
		}

		//Добавление сотрудников указанных в project.EmployeeIds в проект
		for _, eid := range project.EmployeeIds {
			err = p.AddEmployee(int(id), eid)
			if err != nil {
				return err
			}
		}
	} else { //Иначе сохраняем информацию о проекте с указанным идентификатором
		_, err := app.Db.Exec("UPDATE projects SET name=$2, description=$3 WHERE id=$1", project.Id, project.Name, project.Description)
		if err != nil {
			return err
		}
	}

	return nil
}

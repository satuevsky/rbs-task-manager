package controllers

/**
Функция для возврата ошибки
*/
func getError(error error) map[string]string {
	return map[string]string{"error": error.Error()}
}

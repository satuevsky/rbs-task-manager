package controllers

import (
	"encoding/json"
	"github.com/revel/revel"
	"rbs-task-manager/app/models"
	"rbs-task-manager/app/models/entities"
)

/**
Контроллер для работы с задачами
*/
type CTasks struct {
	*revel.Controller
	model models.TaskModel
}

/**
POST /api/tasks/saveTask
Метод для добавления/редактирования задачи
*/
func (c CTasks) SaveTask() revel.Result {
	var task entities.Task

	//Считываем тело запроса в task
	if err := json.Unmarshal(c.Params.JSON, &task); err != nil {
		return c.RenderJSON(getError(err))
	}

	//Сохравняем задачу в бд
	if err := c.model.Save(task); err != nil {
		return c.RenderJSON(getError(err))
	}

	return c.RenderJSON("ok")
}

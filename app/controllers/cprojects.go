package controllers

import (
	"encoding/json"
	"github.com/revel/revel"
	"rbs-task-manager/app/models"
	"rbs-task-manager/app/models/entities"
	"strconv"
)

/**
Контроллер апи для работы с проектами
*/
type CProjects struct {
	*revel.Controller
	model models.ProjectModel
}

/**
GET /api/projects
Метод для получения списка всех проектов
*/
func (c CProjects) Index() revel.Result {
	var projects, _ = c.model.GetAll()
	return c.RenderJSON(projects)
}

/**
GET /api/projects/getProject/:id
Метод для получения конкретного проекта со списком сотрудников и задач
*/
func (c CProjects) GetProject() revel.Result {
	i, err := strconv.Atoi(c.Params.Route.Get("id"))
	if err != nil {
		return c.RenderJSON(getError(err))
	}

	project, err := c.model.GetProject(i)
	if err != nil {
		return c.RenderJSON(getError(err))
	}

	return c.RenderJSON(project)
}

/**
POST /api/projects/saveProject
Метод для сохранения(добавление/редактирование) проекта
*/
func (c CProjects) SaveProject() revel.Result {
	var project entities.ProjectWithEmployeeIds

	data := c.Params.JSON
	err := json.Unmarshal(data, &project)
	if err != nil {
		return c.RenderJSON(getError(err))
	}

	c.model.SaveProject(project)

	return c.RenderJSON("ok")
}

/**
POST /api/projects/addEmployee/:projectId/:employeeId
Метод для добавления сотрудника в проект
*/
func (c CProjects) AddEmployee() revel.Result {
	projectId, err1 := strconv.Atoi(c.Params.Route.Get("projectId"))
	employeeId, err2 := strconv.Atoi(c.Params.Route.Get("employeeId"))

	if err1 != nil {
		return c.RenderJSON(getError(err1))
	}
	if err2 != nil {
		return c.RenderJSON(getError(err2))
	}

	err := c.model.AddEmployee(projectId, employeeId)
	if err != nil {
		return c.RenderJSON(getError(err))
	}

	return c.RenderJSON("ok")
}

/**
DELETE /api/projects/removeEmployee/:projectId/:employeeId
Метод для удаления сотрудника из проекта
*/
func (c CProjects) RemoveEmployee() revel.Result {
	projectId, err1 := strconv.Atoi(c.Params.Route.Get("projectId"))
	employeeId, err2 := strconv.Atoi(c.Params.Route.Get("employeeId"))

	if err1 != nil {
		return c.RenderJSON(getError(err1))
	}
	if err2 != nil {
		return c.RenderJSON(getError(err2))
	}

	err := c.model.RemoveEmployee(projectId, employeeId)
	if err != nil {
		return c.RenderJSON(getError(err))
	}

	return c.RenderJSON("ok")
}

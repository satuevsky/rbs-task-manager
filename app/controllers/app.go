package controllers

import (
	"github.com/revel/revel"
)

type App struct {
	*revel.Controller
}

/*
 GET /
 Возвращает страницу приложения (webix версия)
*/
func (c App) Index() revel.Result {
	return c.Render()
}

/*
 Возвращает страницу приложения (версия без использования библиотек)
*/
func (c App) Native() revel.Result {
	return c.Render()
}

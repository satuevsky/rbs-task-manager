package controllers

import (
	"encoding/json"
	"github.com/revel/revel"
	"rbs-task-manager/app/models"
	"rbs-task-manager/app/models/entities"
	"strconv"
)

/**
Контроллер апи для работы с сотрудниками
*/
type CEmployees struct {
	*revel.Controller
	model models.EmployeeModel
}

/*
GET /api/employees
Возвращает список сотрудников
*/
func (c CEmployees) Index() revel.Result {
	var employees, err = c.model.GetAll()
	if err != nil {
		return c.RenderJSON(getError(err))
	}

	return c.RenderJSON(employees)
}

/*
POST /api/employees/saveEmployee
Метод для добавления/редактирования сотрудника
*/
func (c CEmployees) SaveEmployee() revel.Result {
	var employee entities.Employee

	//Считывание тела запроса в employee
	err := json.Unmarshal(c.Params.JSON, &employee)
	if err != nil {
		return c.RenderJSON(getError(err))
	}

	err = c.model.Save(employee)
	if err != nil {
		return c.RenderJSON(getError(err))
	}

	return c.RenderJSON("ok")
}

/*
DELETE /api/employees/removeEmployee/:id
Метод для удаления сотрудника по указанному id
*/
func (c CEmployees) RemoveEmployee() revel.Result {
	var id, err = strconv.Atoi(c.Params.Route.Get("id"))
	if err != nil {
		return c.RenderJSON(getError(err))
	}

	err = c.model.Remove(id)
	if err != nil {
		return c.RenderJSON(getError(err))
	}

	return c.RenderJSON("ok")
}

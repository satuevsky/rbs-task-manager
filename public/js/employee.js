/**
 * Конструктор для сущности Сотрудник
 * @param {Object} data
 * @param {Number} data.id - Идентификатор сотрудника
 * @param {String} data.name - ФИО сотрудника
 * @param {String} data.bdate - Дата рождения сотрудника
 * @param {String} data.phone - Телефон сотрудника
 * @returns {Employee}
 * @constructor
 */
function Employee(data) {
    if(!(this instanceof Employee))
        return new Employee(data);

    this.id = data.id;
    this.name = data.name;
    this.bdate = new Date(data.bdate);
    this.phone = data.phone;


    /**
     * Отправляет запрос на сохранение данного сотрудника
     * @param callback
     */
    this.save = function(callback){
        api.saveEmployee(this, callback);
    };


    /**
     * Отправляет запрос на удаление данного сотрудника
     * @param callback
     */
    this.remove = function(callback){
        api.removeEmployee(this.id, callback);
    }
}


/**
 * Загружает список сотрудников
 * @param callback
 */
Employee.loadEmployees = function(callback){
    api.getEmployees((err, data) => {
        if(err){
            callback(err);
        }else{
            callback(null, data.map(Employee));
        }
    });
};


/**
 * Создает нового сотрудника
 * @param {Object} data
 * @param {String} data.name - ФИО сотрудника
 * @param {String} data.bdate - Дата рождения сотрудника
 * @param {String} data.phone - Телефон сотрудника
 * @param callback
 */
Employee.create = function(data, callback){
    api.saveEmployee(data, (err, employee) => {
        if(err){
            callback(err);
        }else{
            callback(null, new Employee(employee));
        }
    });
};
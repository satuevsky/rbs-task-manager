/**
 * Конструктор для сущности Задача
 * @param {Object} data
 * @param {Number} data.id - Идентификатор задачи.
 * @param {String} data.name - Имя задачи.
 * @param {String} data.description - Описание задачи.
 * @param {String} data.status - Статус задачи.
 * @param {Employee} data.employee - Сотрудник, которому назначена задача.
 * @param {Project} data.project - Проект, в котором создана задача.
 * @constructor
 */
function Task(data){
    this.id = data.id;
    this.name = data.name;
    this.description = data.description;
    this.status = data.status;
    this.employee = data.employee;
    this.project = data.project;

    /**
     * Отправляет запрос на сохранение задачи.
     * @param callback
     */
    this.save = function(callback){
        api.saveTask(this, callback);
    }
}


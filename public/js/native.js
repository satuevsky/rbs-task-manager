/**
 * Класс для работы c UI.
 * @constructor
 */
function AppUI(){
    let self = this,
        elems = {
            projectsList: byId('projects-list'),
            projectTasksTitle: byId('project-tasks-title'),
            projectTasksList: byId('tasks-list'),
            projectEmployeesList: byId('employees-list'),
            employeesDialog: byId('employees-dialog'),
            employeesList: byId('all-employees-list'),
            employeeCreateForm: byId('employee-create-form'),
        },
        selectedProject = null;


    //close-btn это класс, предназначенный для кнопок, которые закрывают диалоговые окна.
    //Находим все элементы с этим классов и вешаем на них логику закрытия окна.
    Array.prototype.slice.call(document.getElementsByClassName('close-btn')).forEach(btn => {
        btn.addEventListener('click', () => {btn.parentNode.classList.remove('show')});
    });


    //кнопка для открытия диалогового окна со списком всех сотрудников.
    byId('all-employees-btn').addEventListener('click', () => {
        self.updateEmployeesList();
        self.showEmployeesDialog(true);
    });
    //кнопка для открытия диалогового окна позволяющее добавлять сотрудников.
    byId('add-employee-btn').addEventListener('click', () => {
        self.showEditEmployeeDialog(true);
    });
    //кнопка для открытия диалогового окна позволяющее создавать проект.
    byId('add-project-btn').addEventListener('click', () => {
        self.showProjectDialog(true);
    });
    //кнопка для открытия диалогового окна позволяющее добавлять сотрудника в проект.
    byId('add-employee-to-project-btn').addEventListener('click', () => {
        self.showAddEmployeeToProjectDialog(true);
    });
    //кнопка для открытия диалогового окна позволяющее добавлять задачу в проект.
    byId('add-task-btn').addEventListener('click', () => {
        self.showTaskDialog(true);
    });


    //обработка формы, для создания сотрудника.
    byId('employee-create-form').addEventListener('submit', (e) => {
        e.preventDefault();
        let dialog = byId('add-employees-dialog'),
            employee = dialog.employee;

        employee.name = byId('name').value;
        employee.bdate = new Date(byId('bdate').value);
        employee.phone = byId('phone').value;

        self.showEditEmployeeDialog(false);

        if(employee instanceof Employee){
            employee.save(callback);
        }else{
            Employee.create(employee, callback);
        }

        function callback(){
            self.updateEmployeesList();
        }
    });
    //обработка формы, для добавления проекта.
    byId('add-project-form').addEventListener('submit', (e) => {
        e.preventDefault();
        let dialog = byId('add-project-dialog');
        dialog.project.name = byId('project-name-input').value;
        Project.create(dialog.project, () => {
            self.updateProjects();
            self.showProjectDialog(false);
        });
    });
    //обработка формы, для добавления сотрудника в проект.
    byId('add-employee-to-project-form').addEventListener('submit', (e) => {
        e.preventDefault();
        let select = byId('add-employee-select');
        selectedProject.addEmployee(select.value, () => {
            self.updateProjectInfo();
            self.showAddEmployeeToProjectDialog(false);
        });
    });
    //обработка формы, для редактирования задачи.
    byId('task-edit-form').addEventListener('submit', (e) => {
        e.preventDefault();
        let dialog = byId('task-dialog'),
            task = dialog.task;

        task.name = byId('task-name-input').value;
        task.status = byId('task-status-input').value;
        task.employeeId = byId('task-employee-input').value;
        task.projectId = selectedProject.id;

        if(task instanceof Task){
            task.save(callback);
        }else{
            selectedProject.createTask(task, callback)
        }

        function callback(){
            self.updateProjectInfo();
            self.showTaskDialog(false);
        }
    });


    //Обновление бокового списка с проектов.
    this.updateProjects = () => {
        Project.updateProjectsList((err, projects) => {
            if(!selectedProject){
                selectProject(projects[0]);
            }
            elems.projectsList.innerHTML = "";
            projects.forEach(p => {
                let projectItem = ce('div');
                projectItem.classList.add("project-item");
                if(selectedProject && selectedProject.id === p.id) projectItem.classList.add('selected');
                projectItem.innerHTML = "#"+p.id + " " + p.name;
                projectItem.addEventListener("click", () => {
                    selectProject(p);
                });
                elems.projectsList.appendChild(projectItem);
            });
        });

    };
    //Обновление таблиц с задачами и сотрудниками выбранного проекта.
    this.updateProjectInfo = () => {
        elems.projectTasksList.innerHTML = "";
        elems.projectEmployeesList.innerHTML = "";

        elems.projectTasksTitle.innerHTML = "Задачи для проекта '"+ selectedProject.name +"'";

        selectedProject.loadAllData((err, {tasks, employees}) => {
            //рисуем таски
            if(tasks.length){
                tasks.forEach(t => {
                    let tr = ce('tr');
                    tr.innerHTML = `<td><a>${t.name}</a></td><td>${t.status}</td><td>${t.employee ? t.employee.name : "нет сотрудника"}</td>`;
                    tr.getElementsByTagName('td')[0].addEventListener('click', () => {
                        self.showTaskDialog(true, t);
                    });
                    elems.projectTasksList.appendChild(tr);
                });
            }else{
                let tr = ce('tr');
                tr.innerHTML = `<td colspan="3" style="text-align: center" >Нет задач</td>`;
                elems.projectTasksList.appendChild(tr);
            }

            //рисуем сотрудников
            if(employees.length){
                employees.forEach(e => {
                    let tr = ce('tr');
                    tr.innerHTML = `<td colspan="3">${e.name}</td><td><a>удалить</a></td>`;
                    tr.getElementsByTagName('a')[0].addEventListener('click', () => {
                        selectedProject.removeEmployee(e.id, (err) => {
                            if(err){
                                alert(err.message);
                            }else{
                                self.updateProjectInfo();
                            }
                        });
                    });
                    elems.projectEmployeesList.appendChild(tr);
                });
            }else{
                let tr = ce('tr');
                tr.innerHTML = `<td colspan="3" style="text-align: center" >Нет сотрудников</td>`;
                elems.projectEmployeesList.appendChild(tr);
            }

        });
    };
    //Обновление таблицы со списком сотрудников.
    this.updateEmployeesList = () => {
        Employee.loadEmployees((err, employees) => {
            elems.employeesList.innerHTML = "";
            if(!employees.length){
                let tr = ce('tr');
                tr.innerHTML = '<td colspan="4" style="text-align: center">Нет сотрудников</td>';
                elems.employeesList.appendChild(tr);
            }else{
                employees.forEach(employee => {
                    let tr = ce('tr');
                    tr.innerHTML = `
                    <td>${employee.name}</td>
                    <td>${employee.bdate.toDateString()}</td>
                    <td>${employee.phone}</td>
                    <td>
                        <a>редактировать</a>
                        <a>удалить</a>
                    </td>`;
                    let aa = tr.getElementsByTagName('a');

                    aa[0].addEventListener('click', () => {
                        self.showEditEmployeeDialog(true, employee);
                    });
                    aa[1].addEventListener('click', () => {
                        employee.remove((err) => {
                            if(err){
                                alert(err.message);
                            }else{
                                self.updateEmployeesList();
                                self.updateProjectInfo();
                            }
                        });
                    });
                    elems.employeesList.appendChild(tr);
                });
            }

        });
    };


    //Показ диалогового окна, для создания проекта.
    this.showProjectDialog = (show) => {
        let dialog = byId('add-project-dialog');
        if(show){
            dialog.classList.add('show');
            dialog.project = {employeeIds: []};

            let employeeInputs = byId('project-employee-inputs');
            employeeInputs.innerHTML = "";

            Employee.loadEmployees((err, employees) => {
                employees.forEach(e => {
                    let input = document.createElement('input');
                    input.type = 'checkbox';
                    input.addEventListener('change', () => {
                        if(input.checked){
                            dialog.project.employeeIds.push(e.id)
                        }else{
                            dialog.project.employeeIds = dialog.project.employeeIds.filter(_e => _e !== e.id);
                        }
                    });
                    employeeInputs.appendChild(input);
                    employeeInputs.appendChild(ce('span', e.name));
                    employeeInputs.appendChild(ce('br'));
                });
            });
        }else{
            dialog.classList.remove('show');
        }
    };
    //Показ диалогового окна со списком сотрудников.
    this.showEmployeesDialog = (show) => {
        if(show){
            elems.employeesDialog.classList.add('show');
        }else{
            elems.employeesDialog.classList.remove('show');
        }
    };
    //Показ диалогового окна, для редактирования сотрудника.
    this.showEditEmployeeDialog = (show, employee) => {
        let dialog = byId('add-employees-dialog');
        if(show){
            dialog.classList.add('show');
            dialog.employee = employee = employee || {};
            byId('name').value = employee.name || "";
            byId('bdate').value = employee.bdate ? employee.bdate.toISOString().substr(0,10) : "";
            byId('phone').value = employee.phone || "";
        }else{
            dialog.classList.remove('show');
        }
    };
    //Показ диалогового окна, для добавления сотрудника в проект.
    this.showAddEmployeeToProjectDialog = (show) => {
        let dialog = byId('add-employee-to-project-dialog');
        if(show){
            dialog.classList.add('show');
            let select = byId('add-employee-select');
            select.innerHTML = '';
            Employee.loadEmployees((err, employees) => {
                select.innerHTML = employees
                    .filter(e => !selectedProject.employees.some(pe => pe.id === e.id))
                    .map(e => `<option value="${e.id}">${e.name}</option>`)
                    .join('');
            });
        }else{
            dialog.classList.remove('show');
        }
    };
    //Показ диалогового окна, для редактирования/создания задачи.
    this.showTaskDialog = (show, task) => {
        let dialog = byId('task-dialog');
        if(show){
            dialog.classList.add('show');
            dialog.task = task = task || {};

            byId('task-name-input').value = task.name || "";
            byId('task-status-input').value = task.status || "Назначена";

            selectedProject.loadAllData((err, {employees}) => {
                let employeeSelect = byId('task-employee-input'),
                    employeeId = task.employee && task.employee.id;
                employeeSelect.innerHTML = employees
                    .map(e => `<option value="${e.id}" ${e.id === employeeId ? 'selected' : ''}>${e.name}</option>`)
                    .join('');
            });
        }else{
            dialog.classList.remove('show');
        }
    };


    function selectProject(p){
        selectedProject = p;
        self.updateProjects();
        self.updateProjectInfo();
    }
}



function ce(tag, innerHTML){
    let el = document.createElement(tag);
    if(innerHTML)el.innerHTML = innerHTML;
    return el;
}
function byId(id){
    return document.getElementById(id);
}

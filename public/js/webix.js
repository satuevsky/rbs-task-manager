function WebixApp(){
    let self = this;

    this.start = () => {
        initWebixConfig();
        attachEventListeners();
        this.updateProjectsList();
    };

    //Перерисовка списка проектов.
    this.updateProjectsList = () => {
        Project.updateProjectsList((err, list) => {
            $$("projects_list").parse(Project.projectsList);
            if(!$$("projects_list").getSelectedItem() && list.length){
                $$("projects_list").select(list[0].id);
            }
        });
    };

    //Перерисовка информации о выбранном проекте(задачи, сотрудники).
    this.updateContent = () => {
        //Загружает задачи и сотрудников выбранного проекта
        $$("projects_list").getSelectedItem().loadAllData((err, {employees, tasks}) => {
            $$("tasks_table").clearAll();
            $$("tasks_table").parse(tasks.length ? tasks : [{name: "-", status: "-", employee: {name: "-"}}]);

            $$("employees_table").clearAll();
            $$("employees_table").parse(employees.length ? employees : [{name: "-", status: "-", employee: {name: "-"}}]);
        });
    };

    //Перерисовка списка со всеми сотрудниками
    this.updateAllEmployees = () => {
        Employee.loadEmployees((err, employees) => {
            $$('all_employees_table').clearAll();
            $$('all_employees_table').parse(employees);
        });
    };

    /**
     * Показ окна для редактирования задачи
     * @param {Task} [task] - Задача для редактирования. Если не указана, то будет создана новая.
     */
    this.showTaskEditor = (task) => {
        let isNewTask = $$('edit_task_form').isNewTask = !task;
        $$('edit_task_form').task = task = task || {};

        //Перерисовка заголовка окна.
        $$('edit_task_title').define('label', isNewTask ? 'Новая задача' : 'Редактирование задачи');
        $$('edit_task_title').refresh();

        $$('edit_task_form').setValues({
            name: task.name || "",
            status: task.status || "Назначена"
        });

        $$('edit_task_window').show();

        $$("projects_list").getSelectedItem().loadAllData((err, {employees}) => {
            //Обновляем список сотрудников для выбора.
            $$('edit_task_employee').define('options', employees.map(e => ({id: e.id, value: e.name})));
            $$('edit_task_employee').refresh();

            //Ставим по умолчанию сотрудника который указан в задаче(редактирование) или первого в списке(создание)
            $$('edit_task_employee').setValue(task.employee ? task.employee.id : (employees.length && employees[0].id));
        });
    };
    /**
     * Показ окна для редактирования сотрудника
     * @param {Employee} [employee] - Сотрудник для редактирования. Если не указан, то будет добавлен новый.
     */
    this.showEmployeeEditor = (employee) => {
        let isNew = $$('edit_task_form').isNew = !employee;
        $$('edit_employee_form').employee = employee = employee || {};

        //Перерисовка заголовка окна.
        $$('edit_employee_title').define('label', isNew ? 'Новый сотрудник' : 'Редактирование');
        $$('edit_employee_title').refresh();

        $$('edit_employee_form').setValues({
            name: employee.name || "",
            phone: employee.phone || "",
            bdate: employee.bdate || ""
        });

        $$('edit_employee_window').show();
    };

    /**
     * Показ окна для редактирования проекта
     * @param {Project} [project] - Проект для редактирования. Если не указан, то будет создан новый.
     */
    this.showProjectEditor = (project) => {
        let isNew = $$('edit_project_form').isNew = !project;
        $$('edit_project_form').project = project = project || {};

        //Перерисовка заголовка окна.
        $$('edit_project_title').define('label', isNew ? 'Новый проект' : 'Редактирование');
        $$('edit_project_title').refresh();

        $$('edit_project_form').setValues({
            name: project.name || "",
            description: project.description || "",
        });

        $$('edit_project_employees_table').clearAll();
        if(isNew){
            $$('edit_project_employees_table').show();
            $$('edit_project_employees_label').show();
            Employee.loadEmployees((err, employees) => {
                $$('edit_project_employees_table').clearAll();
                $$('edit_project_employees_table').parse(employees.map(employee => ({checked: 0, employee})));
            });
        }else{
            $$('edit_project_employees_table').hide();
            $$('edit_project_employees_label').hide();
        }

        $$('edit_project_window').show();
    };


    //Вешаем обработчики событий на компоненты
    function attachEventListeners(){
        //При клике по проекту
        $$("projects_list").attachEvent('onAfterSelect', id => {
            let project = $$("projects_list").getItem(id);
            //Обновление заголовка
            $$('tasks_label').define('label', `Задачи проекта '${project.name}'`);
            $$('tasks_label').refresh();

            self.updateContent();
        });

        //При клике по задаче
        $$('tasks_table').attachEvent('onItemClick', (id) => {
            self.showTaskEditor($$('tasks_table').getItem(id));
        });

        //При показе окна для добавления сотрудника в проект
        $$('add_employee_to_project_window').attachEvent('onBeforeShow', () => {
            Employee.loadEmployees((err, employees) => {
                let currentEmployees = $$('employees_table').serialize(),
                    availableEmployees = employees
                        .filter(e1 => !currentEmployees.some(e2 => e1.id === e2.id));

                $$('add_employee_select').define('options', availableEmployees.map(employee => ({id: employee.id, value: employee.name})));
                $$('add_employee_select').setValue(availableEmployees.length ? availableEmployees[0].id : undefined);
            });
        });

        //Кнопка для показа окна добавления задачи
        $$('add_task_btn').attachEvent('onItemClick', () => {
            self.showTaskEditor();
        });

        //Кнопка создания проекта
        $$('save_project_btn').attachEvent('onItemClick', () => {
            if(!$$('edit_project_form').validate())return;

            let form = $$('edit_project_form'),
                project = form.project,
                values = form.getValues();

            project.name = values.name;
            project.description = values.description;

            if(project instanceof Project){
                project.save(self.updateProjectsList);
            }else{
                project.employeeIds = $$('edit_project_employees_table').serialize()
                    .filter(item => item.checked)
                    .map(item => item.employee.id);
                Project.create(project, self.updateProjectsList);
            }

            $$('edit_project_window').hide();
        });

        //Кнопка добавления сотрудника в проект
        $$('save_employee_to_project_btn').attachEvent('onItemClick', () => {
            let employeeId = $$('add_employee_to_project_form').getValues().employeeId;

            if(!employeeId)return;

            $$('projects_list').getSelectedItem().addEmployee(employeeId, (err) => {
                self.updateContent();
            });

            $$('add_employee_to_project_window').hide();
        });
        //Кнопка сохранения задачи
        $$('save_task_btn').attachEvent('onItemClick', () => {
            if(!$$('edit_task_form').validate())return;

            console.log();

            let form = $$('edit_task_form'),
                task = form.task,
                values = form.getValues();

            task.name = values.name;
            task.status = values.status;
            task.employeeId = values.employeeId;
            task.projectId = $$("projects_list").getSelectedItem().id;

            if(task instanceof Task){
                task.save(onSave)
            }else{
                $$("projects_list").getSelectedItem().createTask(task, onSave)
            }

            function onSave(){
                self.updateContent()
            }

            $$('edit_task_window').hide();
        });
    }
    function initWebixConfig(){
        //конфигурация хэдера
        let header = {view: "toolbar", elements: [{view: "label", label: "TaskManager"}, {view: "button", value: "Все сотрудники", width: 130, click: "$$('all_employees_window').show()"}]},
            //конфигурация левого меню со списком проектов
            projects_col = {
                width: 200,
                rows: [
                    {
                        view: "toolbar", elements: [
                            {view: "label", label: "Проекты"},
                            {view: "button", type: "icon", icon: "pencil", width: 30, click: () => self.showProjectEditor($$('projects_list').getSelectedItem())},
                            {view: "button", type: "icon", icon: "plus", width: 30, click: () => self.showProjectEditor()},
                        ]},
                        {id: "projects_list", view: "list", template: "#name#", data: Project.projectsList, select: true}
                ]
            },
            //конфигурация списка задач выбранного проекта
            tasks_row =  {rows: [
                {view: "toolbar", elements: [{id: "tasks_label", view: "label", label: "Задачи"}, {id: "add_task_btn", view: "button", value: "добавить", width: 90}]},
                {
                    id: "tasks_table",
                    view: "datatable",
                    //header: false,
                    minHeight: 250,
                    columns: [
                        {id: "name", header: "Наименование", fillspace: 1},
                        {id: "status", header: "Статус", fillspace: 1},
                        {header: "Назначена на", template: t => t.employee.name, fillspace: 1}],
                    data: [],
                }
            ]},
            //конфигурация списка задач сотрудников проекта
            employees_row = {rows: [
                {view: "toolbar", elements: [
                    {view: "label", label: "Сотрудники"},
                    {view: "button", value: "удалить", width: 90, id: "remove_employee_from_project", hidden: true, on: {
                        onItemClick: () => {
                            let emp = $$('employees_table').getSelectedItem();
                            if(emp){
                                $$('projects_list').getSelectedItem().removeEmployee(emp.id, (err) => {
                                    if(err){
                                        webix.alert({title: "Ошибка", text: err.message, type:"alert-error"});
                                    }else{
                                        self.updateContent();
                                    }
                                });
                            }
                        }
                    }},
                    {view: "button", value: "добавить", width: 90, click: '$$("add_employee_to_project_window").show()'}
                ]},
                {
                    id: "employees_table",
                    view: "datatable",
                    minHeight: 250,
                    select: true,
                    columns: [{id: "name", header: "ФИО", fillspace: 1}],
                    data: [],
                    on: {
                        onSelectChange: () => {
                            if($$('employees_table').getSelectedItem()){
                                $$('remove_employee_from_project').show();
                            }else{
                                $$('remove_employee_from_project').hide();
                            }
                        }
                    }
                }
            ]},
            //конфигурация окна дабавления проекта
            edit_project_window = {
                id: "edit_project_window",
                view: "window",
                width: 400,
                //height: 370,
                modal: true,
                position: "center",
                head: {
                    view: "toolbar", elements: [{id: "edit_project_title", view: "label", label: "Добавление проекта"}, {view: "button", value: "Закрыть", width: 80, click: "$$('edit_project_window').hide();"}]
                },
                body: {
                    //view: "scrollview",
                    //body: {
                        view: "form",
                        id: "edit_project_form",
                        elements: [
                            {view: "text", placeholder: "Название проекта", name: "name", required: true},
                            {view: "textarea", placeholder: "Описание проекта", name: "description"},
                            {id: "edit_project_employees_label", view: "label", label: "Сотрудники для проекта"},
                            {
                                id: "edit_project_employees_table",
                                view: "datatable",
                                scroll: "y",
                                height: 120,
                                header: false,
                                columns: [{id: "checked", template: "{common.checkbox()}", width: 42}, {id: "name", template: r => r.employee.name, fillspace: true}],
                                data: [],
                                on: {
                                    onItemClick:function(id){
                                        this.getItem(id.row).checked = !this.getItem(id).checked;
                                        this.refresh(id.row);
                                    }
                                }
                            },
                            {id: "save_project_btn", view: "button", value: "Добавить"},
                        ]
                    //}
                }
            },
            add_task_window = {
                id: "edit_task_window",
                view: "window",
                width: 400,
                //height: 260,
                modal: true,
                position: "center",
                head: {
                    view: "toolbar", elements: [{id: "edit_task_title", view: "label", label: "Задача"}, {view: "button", value: "Закрыть", width: 80, click: "$$('edit_task_window').hide();"}]
                },
                body: {
                    view: "form",
                    id: "edit_task_form",
                    elements: [
                        {view: "text", placeholder: "Название задачи", name: "name", required: true},
                        {view: "richselect", options: ["Назначена", "В работе", "Решена"], name: "status", required: true},
                        {id: "edit_task_employee", view: "richselect", options: [], name: "employeeId", required: true},
                        {id: "save_task_btn", view: "button", value: "Сохранить"},
                    ]
                }
            },
            add_employee_to_project_window = {
                id: "add_employee_to_project_window",
                view: "window",
                width: 400,
                //height: 260,
                modal: true,
                position: "center",
                head: {
                    view: "toolbar", elements: [{view: "label", label: "Добавление сотрудника"}, {view: "button", value: "Закрыть", width: 80, click: "$$('add_employee_to_project_window').hide();"}]
                },
                body: {
                    view: "form",
                    id: "add_employee_to_project_form",
                    elements: [
                        {id: "add_employee_select", view: "richselect", options: [], name: "employeeId", required: true},
                        {id: "save_employee_to_project_btn", view: "button", value: "Сохранить"},
                    ]
                }
            },
            all_employees_window = {
                id: "all_employees_window",
                view: "window",
                width: 600,
                //height: 260,
                modal: true,
                position: "center",
                head: {
                    view: "toolbar", elements: [
                        {view: "label", label: "Все сотрудники"},
                        {id: 'remove_employee_btn', view: "button", value: "Удалить", width: 90, hidden: true, click: () => {
                                $$('all_employees_table').getSelectedItem().remove((err) => {
                                    if(err){
                                        webix.alert({title: "Ошибка", text: err.message, type:"alert-error"});
                                    }else{
                                        self.updateAllEmployees();
                                        self.updateContent();
                                    }
                                })
                            }},
                        {id: 'edit_employee_btn', view: "button", value: "Изменить", width: 90, hidden: true, click: () => self.showEmployeeEditor($$('all_employees_table').getSelectedItem())},
                        {id: "show_add_employee_window_btn", view: "button", value: "Добавить", width: 90, click: () => {self.showEmployeeEditor()}},
                        {view: "button", value: "Закрыть", width: 80, click: "$$('all_employees_window').hide();"}]
                },
                body: {
                    id: 'all_employees_table',
                    view: "datatable",
                    select: true,
                    scroll: "y",
                    columns: [
                        {id: "name",  header: "ФИО", fillspace: 3},
                        {id: "bdate", header: "Дата рождения", template: e => e.bdate.toDateString(), fillspace: 2},
                        {id: "phone" ,header: "Телефон", fillspace: 2}],
                    data: [],
                    on: {onSelectChange: () => {
                        if($$('all_employees_table').getSelectedItem()){
                            $$('remove_employee_btn').show();
                            $$('edit_employee_btn').show();
                        }else{
                            $$('remove_employee_btn').hide();
                            $$('edit_employee_btn').hide();
                        }
                    }}
                },
                on: {onBeforeShow: self.updateAllEmployees}
            },
            edit_employee_window = {
                id: "edit_employee_window",
                view: "window",
                width: 400,
                height: 280,
                modal: true,
                position: "center",
                head: {
                    view: "toolbar", elements: [
                        {id: "edit_employee_title", view: "label", label: "Новый сотрудник"},
                        {view: "button", value: "Закрыть", width: 80, click: "$$('edit_employee_window').hide();"}]
                },
                body: {
                    id: "edit_employee_form",
                    view: "form",
                    elements: [
                        {view: "text", placeholder: "ФИО", name: "name", required: true},
                        {view: "datepicker", label: "Дата рождения", labelPosition: "top", name: "bdate", required: true},
                        {view: "text", placeholder: "Телефон", name: "phone", required: true},
                        {id: "add_employee_btn", view: "button", value: "Сохранить", click: () => {
                            if(!$$('edit_employee_form').validate())return;
                            let form = $$('edit_employee_form'),
                                employee = form.employee,
                                values = form.getValues();

                            employee.name = values.name;
                            employee.phone = values.phone;
                            employee.bdate = values.bdate;

                            if(employee instanceof Employee){
                                employee.save(self.updateAllEmployees);
                            }else{
                                Employee.create($$('edit_employee_form').getValues(), self.updateAllEmployees);
                            }

                            $$('edit_employee_window').hide();
                        }},
                    ]
                }
            };


        //конфигурация макета
        webix.ui({
            view: "scrollview",
            scroll: "y",
            body: {rows: [header, {type: "space", cols: [projects_col, {
                rows: [tasks_row, employees_row]
            }]}]}
        });

        //добавление диалоговых окон
        webix.ui(edit_project_window);
        webix.ui(add_task_window);
        webix.ui(add_employee_to_project_window);
        webix.ui(all_employees_window);
        webix.ui(edit_employee_window);
    }
}

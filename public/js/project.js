Project.projectsList = [];

/**
 * Конструктор для создания объекта проекта
 * @param {Object} data
 * @param {Number} data.id - Идентификатор проекта
 * @param {String} data.name - Наименование проекта
 * @param {String} data.description - Описание проекта
 * @returns {Project}
 * @constructor
 */
function Project(data){
    if(!(this instanceof Project))
        return new Project(data);

    this.id = data.id;
    this.name = data.name;
    this.description = data.description;

    /**
     * Сохраняет информацию о проекте
     * @param callback
     */
    this.save = function(callback){
        api.saveProject(this, callback);
    };

    /**
     * Загружает всю информацию о проекте(задачи, сотрудники)
     * @param callback
     */
    this.loadAllData = function(callback){
        api.getProject(this.id, (err, data) => {
            if(err){
                callback(err);
            }else{
                let employeesData = data.employees,
                    tasksData = data.tasks,
                    employees = employeesData.map(Employee),
                    tasks = tasksData.map(t => new Task({
                            id: t.id,
                            name: t.name,
                            description: t.description,
                            status: t.status,
                            project: this,
                            employee: employees.find(e => e.id === t.employeeId)
                        }));

                callback(null, {employees, tasks});
            }
        });
    };

    /**
     * Добавление сотрудника на проект
     * @param {Number} employeeId - Идентификатор добавляемого сотрудника.
     * @param {Function} callback
     */
    this.addEmployee = function(employeeId, callback){
        api.addEmployeeToProject(this.id, employeeId, (err) => {
            callback(err);
        });
    };

    /**
     * Удаление сотрудника с проекта
     * @param {Number} employeeId - Удаляемый сотрудник
     * @param {Function} callback
     */
    this.removeEmployee = function(employeeId, callback){
        api.removeEmployeeFromProject(this.id, employeeId, (err) => {
            callback(err);
        });
    };

    /**
     * Создание задачи.
     * @param {Object} task - Информация о добавляемой задаче.
     * @param {Function} callback
     */
    this.createTask = function(task, callback){
        api.saveTask({
            name: task.name,
            description: task.description,
            status: task.status,
            employeeId: task.employeeId,
            projectId: this.id,
        }, callback);
    }
}

/**
 * Метод загружает список проектов через апи
 * @param callback
 */
Project.updateProjectsList = function(callback){
    api.getProjects((err, data) => {
        if(err){
            callback(err);
        }else{
            //Очищаем список проектов
            Project.projectsList.splice(0, Project.projectsList.length);
            //Добавляем загруженные проекты в список
            Array.prototype.push.apply(Project.projectsList, data.map(Project));

            callback(null, Project.projectsList);
        }
    });
};

/**
 * Отправляет запрос для создания нового проекта.
 * @param {Object} data
 * @param {String} data.name - Наименование проекта
 * @param {String} data.description - Описание проекта
 * @param {[Number]} data.employeeIds - Идентификаторы сотрудников, назначенных на проект.
 * @param callback
 */
Project.create = function(data, callback){
    api.saveProject(data, (err, p) => {
        if(err){
            callback(err);
        }else{
            callback(null, new Project(p));
        }
    });
};
function checkError(json){
    if(json.error){
        throw new Error(json.error)
    }
    return json
}

/**
 * Выполняет GET запрос
 * @param url
 * @returns {Promise<any>}
 * @constructor
 */
function GET(url){
    return fetch(url).then(r => r.json()).then(checkError);
}

/**
 * Выполняет POST запрос
 * @param {String} url
 * @param {*} [json]
 * @returns {Promise<any>}
 * @constructor
 */
function POST(url, json){
    return fetch(url, {
        method: "POST",
        body: JSON.stringify(json),
        headers: {"content-type": "application/json"}
    }).then(r => r.json()).then(checkError);
}

/**
 * Выполняет DELETE запрос по указанному url
 * @param {String} url
 * @param {...String} []
 * @returns {Promise<any>}
 * @constructor
 */
function DELETE(url){
    url = Array.prototype.join.call(arguments, '/');
    return fetch(url, {method: "DELETE"}).then(r => r.json()).then(checkError);
}



//апи для осуществления запросов на сервер(на данный момент к объекту store)
let api = {
    getProjects: (callback) => {
        GET('/api/projects')
            .then(projects => {
                callback(null, projects);
            });
    },
    getProject: (projectId, callback) => {
        GET('/api/projects/getProject/'+projectId)
            .then(project => {
                callback(null, project);
            });
    },
    getEmployees: (callback) => {
        GET('/api/employees')
            .then(employees => {
                callback(null, employees);
            });
    },
    saveProject: (project, callback) => {
        POST('/api/projects/saveProject', project)
            .then(r => callback(null, r));
    },
    saveEmployee: (employee, callback) => {
        POST('/api/employees/saveEmployee', employee)
            .then(r => callback(null, r));
    },
    saveTask: (task, callback) => {
        POST('/api/tasks/saveTask', task)
            .then(r => callback(null, r));
    },
    addEmployeeToProject: (projectId, employeeId, callback) => {
        POST(['/api/projects/addEmployee', projectId, employeeId].join('/'))
            .then(r => callback(null, r));
    },
    removeEmployee: (employeeId, callback) => {
        DELETE('/api/employees/removeEmployee', employeeId)
            .then(r => callback(null, r))
            .catch(callback);
    },
    removeEmployeeFromProject: (projectId, employeeId, callback) => {
        DELETE('/api/projects/removeEmployee', projectId, employeeId)
            .then(r => callback(null, r))
            .catch(callback);
    }
};